<html lang="es">
    <head>
        <link href="../css/bootstrap.css" rel="stylesheet">
    </head>
<?php
//incluyo la función para conectarme a la BBDD
    include('../funciones.php');
    $mysqli = conectaLi();

//esto luego lo cambiamos por el usuario que esté logeado
   

?>

    <div id="test" style="padding:60px;">
    <label>
        SELECCIÓN PREVIA AL TEST
    </label>
    <h3>
        Numero de Preguntas:
    </h3>
    <select id="numPreguntas">
        <option value="30">30</option>
        <option value="45">45</option>
        <option value="60">60</option>
        <option value="85">85</option>
    </select>
    <h3>
        Tema:
    </h3>
    <select id="temaPreguntas">
        <option value="1">30</option>
        <option value="2">45</option>
        <option value="3">60</option>
        <option value="4">85</option>
    </select>
    <h3>
        Dificultad:
    </h3>
    <select id="dificultadPreguntas">
        <option value="MuyFacil">30</option>
        <option value="Facil">45</option>
        <option value="Medio">60</option>
        <option value="Dificil">85</option>
    </select>
    <button class="btn btn-info" onclick="pasarInfo();">EMPEZAR TEST</button>
    </div>
    
<!--esta carga de script de jquery y bootstrap hay que borrarla cuando pongamos este código dentro del marco final-->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script>
        
        function pasarInfo(){

        
        var _limitador = $("#numPreguntas").val();
        
        
        var _tema = $("#temaPreguntas").val();
        
        
        var _dificultad = $("#dificultadPreguntas").val();
        
        
        $('#test').load("creacionTest.php",{
            limitador : _limitador,
            tema : _tema,
            dificultad : _dificultad
        });
    }

    </script>